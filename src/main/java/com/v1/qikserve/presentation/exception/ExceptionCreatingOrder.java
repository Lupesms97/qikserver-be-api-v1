package com.v1.qikserve.presentation.exception;

public class ExceptionCreatingOrder extends RuntimeException {

        public ExceptionCreatingOrder(String message) {
            super(message);
        }
}
