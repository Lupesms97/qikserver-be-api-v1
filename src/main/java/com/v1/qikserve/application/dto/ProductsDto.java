package com.v1.qikserve.application.dto;

public record ProductsDto(
        String id,
        String name,
        int price) {
}
