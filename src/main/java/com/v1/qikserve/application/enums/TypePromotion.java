package com.v1.qikserve.application.enums;

public enum TypePromotion {
    QTY_BASED_PRICE_OVERRIDE,
    BUY_X_GET_Y_FREE,
    FLAT_PERCENT,
    NO_PROMOTION


}
